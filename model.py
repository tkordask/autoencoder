import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


def weights_init(m):
    for param in m.parameters():
        try:
            nn.init.kaiming_normal_(param)
        except:
            nn.init.normal_(param, 0, 0.02)


class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()

        self.conv128to128 = nn.Sequential(
            nn.Conv2d(3, 18, 4, padding=1, bias=True),
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(18, 18, 4, padding=2, bias=True)
        )

        self.conv128to64 = nn.Sequential(
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(18, 144, 4, 2, padding=1, bias=True)
        )

        self.conv64to32 = nn.Sequential(
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(144, 72, 4, 2, padding=1, bias=True),)

        self.conv32to16 = nn.Sequential(
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(72, 36, 4, 2, padding=1, bias=True),
        )

        self.conv16to8 = nn.Sequential(
            nn.LeakyReLU(inplace=True),
            nn.Conv2d(36, 9, 4, 2, padding=1, bias=True),)

    def forward(self, x):
        out128 = self.conv128to128(x)
        out64 = self.conv128to64(out128)
        out32 = self.conv64to32(out64)
        out16 = self.conv32to16(out32)
        out8 = self.conv16to8(out16)
     #   print(out.size())
        return (out8, out128[:, 0:2, :, :], out64[:, 0:8, :, :], out32[:, 0:24, :, :])

    def init_weights(self):
        self.apply(weights_init)


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()

        self.conv8to16 = nn.Sequential(
            nn.ConvTranspose2d(9, 36, 4, 2, 1, bias=True),
            nn.LeakyReLU(inplace=True))

        self.conv16to32 = nn.Sequential(
            nn.ConvTranspose2d(36, 72, 4, 2, 1, bias=True),
            nn.LeakyReLU(inplace=True))

        self.conv32to64 = nn.Sequential(
            nn.ConvTranspose2d(72+12, 144, 4, 2, 1, bias=True),
            nn.LeakyReLU(inplace=True))

        self.conv64to128 = nn.Sequential(
            nn.ConvTranspose2d(144+4, 144, 4, 2, 1, bias=True),
            nn.LeakyReLU(inplace=True),
        )
        self.conv128to128 = nn.Sequential(
            nn.Conv2d(144+1, 3, 3, padding=1, bias=True),
            nn.Sigmoid())

    def forward(self, x):
        out16 = self.conv8to16(x[0])
        out32 = self.conv16to32(out16)

        means32 = x[3][:, :12, :, :]
        stds32 = torch.exp(x[3][:, 12:, :, :]/2)

        rands32 = stds32.new_empty(stds32.size()).normal_()*stds32 + means32
        out64 = self.conv32to64(torch.cat([out32, rands32], dim=1))

        means64 = x[2][:, :4, :, :]
        stds64 = torch.exp(x[2][:, 4:, :, :]/2)

        rands64 = stds64.new_empty(stds64.size()).normal_()*stds64 + means64
        out128 = self.conv64to128(torch.cat([out64, rands64], dim=1))

        means128 = x[1][:, :1, :, :]
        stds128 = torch.exp(x[1][:, 1:, :, :]/2)

        rands128 = stds128.new_empty(
            stds128.size()).normal_()*stds128 + means128
        out_final = self.conv128to128(torch.cat([out128, rands128], dim=1))

        return out_final

    def init_weights(self):
        self.apply(weights_init)


def num_params(model):
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    return params


enc = Encoder()
dec = Decoder()

enc.init_weights()
dec.init_weights()
print(num_params(enc))
print(num_params(dec))

x = torch.rand((10, 3, 128, 128))
dec(enc(x))
