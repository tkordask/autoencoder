import torch
import torch.optim as opt
import torch.nn as nn
import torch.nn.functional as F
import torchvision.utils as vutils
import numpy as np


def kl_divergence(means, variances):
    return torch.sum(torch.exp(variances)+means**2 - 1 - variances, dim=[1, 2, 3])


def train(enc, dec, data, batch_size, epochs, device, callback, beta=1):
    print("Beginning training!")

    optimizer = opt.Adam(list(enc.parameters())+list(dec.parameters()))

    enc.train()
    dec.train()

    loss = nn.MSELoss()

    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):
            optimizer.zero_grad()

            device_batch = data_batch[0].to(device)

            encoding = enc(device_batch)
            variational_loss_128 = torch.mean(kl_divergence(
                encoding[1][:, :1, :, :], encoding[1][:, 1:, :, :]))
            variational_loss_64 = torch.mean(kl_divergence(
                encoding[2][:, :4, :, :], encoding[2][:, 4:, :, :]))
            variational_loss_32 = torch.mean(kl_divergence(
                encoding[3][:, :12, :, :], encoding[3][:, 12:, :, :]))

            variational_loss = variational_loss_128+variational_loss_64+variational_loss_32

            reconstruction = dec(encoding)

            reconstruction_error = loss(device_batch, reconstruction)

            total_loss = reconstruction_error + beta*variational_loss

            total_loss.backward()
            optimizer.step()

            callback(i, len(data), variational_loss, reconstruction_error,
                     total_loss, enc, dec, epoch, epochs)
